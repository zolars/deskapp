package gnukhata.controllers.reportmodels;

public class netTrialBalance {
	private String srNo;
	private String accountName;
	private String groupName;
	private String drBal;
	private String crBal;
	public netTrialBalance(String srNo, String accountName, String groupName,
			String drBal, String crBal) {
		super();
		this.srNo = srNo;
		this.accountName = accountName;
		this.groupName = groupName;
		this.drBal = drBal;
		this.crBal = crBal;
	}
	/**
	 * @return the srNo
	 */
	public String getSrNo() {
		return srNo;
	}
	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @return the drBal
	 */
	public String getDrBal() {
		return drBal;
	}
	/**
	 * @return the crBal
	 */
	public String getCrBal() {
		return crBal;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.accountName;
	}

}
