package gnukhata.views;

import gnukhata.globals;

import java.util.Vector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class AccountTabForm  extends Composite  {
		static Display display;
		TabFolder tfAccount;
		TabItem tiAddNewAccount;
		Label lblGroupName;
		Combo dropdownGroupName;
		Label lblSubGroupName;
		Combo dropdownSubGroupName;
		Label lblAccountName;
		Text txtAccountName;
		Label lblOpeningBalance; 
		Text txtOpeningBalance;
		Label lblTotalDrOpeningBalance;
		Text txtTotalDrOpeningBalance ;
		Label lblTotalCrOpeningBalance;
		Text txtTotalCrOpeningBalance;
		Label lblDiffInOpeningBalance;
		Text txtDiffInOpeningBalance;
		Combo dropdownGroupName2;
		Button btnSearch;
		Button btnEdit;
		Button btnConfirm;
		Button btnDelete;
		TabItem tiEditAccount;
		Label lblAccountCode;
		Text txtAccountCode;

		
	
		Vector<Object> params;
		protected int[] orgNameList;
		CreateAccountComposite cac;
		FindAndEditAccountComposite fec;
		public AccountTabForm(Composite parent,int style) 
		{
			//super(Display.getDefault());
			super(parent,style);
			FormLayout formlayout = new FormLayout();
			this.setLayout(formlayout);
			
			//this.setText("Gnukhata Startup");
			FormData layout = new FormData();
			
			/*Label lblHeadline = new Label(this,SWT.None);
			lblHeadline.setFont(new Font(display, "Times New Roman", 14, SWT.BOLD));
			lblHeadline.setText("GNUKhata: A Free and Open Source Accounting Software");
			layout = new FormData();
			layout.top = new FormAttachment(2);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(54);
			//layout.bottom = new FormAttachment(10);
			lblHeadline.setLayoutData(layout);*/
			
			Label lblLogo = new Label(this, SWT.None);
			layout = new FormData();
			layout.top = new FormAttachment(1);
			layout.left = new FormAttachment(63);
			layout.right = new FormAttachment(99);
			layout.bottom = new FormAttachment(9);
			//layout.right = new FormAttachment(100);
			//layout.bottom = new FormAttachment(18);
			lblLogo.setLayoutData(layout);
			//Image img = new Image(display,"finallogo1.png");
			lblLogo.setImage(globals.logo);
			
			/*Label lblLink = new Label(this,SWT.None);
			lblLink.setText("www.gnukhata.org");
			lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
			layout = new FormData();
			layout.top = new FormAttachment(lblLogo,1);
			layout.left = new FormAttachment(66);
			//layout.right = new FormAttachment(33);
			//layout.bottom = new FormAttachment(19);
			lblLink.setLayoutData(layout);*/
	
			Label lblOrgDetails = new Label(this,SWT.NONE);
			lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
			lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&")+"\n"+" For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
			layout = new FormData();
			layout.top = new FormAttachment(1);
			layout.left = new FormAttachment(2);
			//layout.right = new FormAttachment(58);
			//layout.bottom = new FormAttachment(18);
			lblOrgDetails.setLayoutData(layout);
			
			Label lblLine = new Label(this,SWT.NONE);
			lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
			layout = new FormData();
			layout.top = new FormAttachment(lblLogo,1);
			layout.left = new FormAttachment(3);
			layout.right = new FormAttachment(99);
			layout.bottom = new FormAttachment(12);
			//layout.right = new FormAttachment(95);
			//layout.bottom = new FormAttachment(22);
			lblLine.setLayoutData(layout);
			
			tfAccount = new TabFolder(this, SWT.NONE);
			layout = new FormData();
			layout.top = new FormAttachment(13);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(95);
			layout.bottom = new FormAttachment(95);
			tfAccount.setLayoutData(layout);
		    //Add New Account Tab
		    tiAddNewAccount = new TabItem(tfAccount, SWT.NONE);
		    tiAddNewAccount.setText("			Add &New Account			");
		    // Find/Edit Account
		    tiEditAccount = new TabItem(tfAccount, SWT.NONE);
		    tiEditAccount.setText("				&Find Account				");
		    
		    this.getAccessible();
			this.pack();
			//this.makeToolBar();
			//this.open();
			this.setEvent();
			//tfAccount.setSelection(tfAccount.getItem(0));
			
			//this.showView();
			}



	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}


	public void setEvent()
	{
		cac = new CreateAccountComposite(tfAccount, SWT.None);
		tiAddNewAccount.setControl(cac);
		
		tfAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if (tfAccount.getSelectionIndex() == 0)
				{
					cac = new CreateAccountComposite(tfAccount, SWT.None);
					tiAddNewAccount.setControl(cac);
					cac.dropdownGroupName.setFocus();
				}
				if (tfAccount.getSelectionIndex() == 1)
				{
					fec = new FindAndEditAccountComposite(tfAccount, SWT.None);
					tiEditAccount.setControl(fec);
					fec.dropdownAllAccounts.setFocus();
				}
				
				
			}
		});
	}
	
	protected void checkSubclass()
	{
		//this is blank method so will disable the check that prevents subclassing of shells.
	}

	/*private void showView()
	{
		while(! this.isDisposed())
		{
			if(! this.getDisplay().readAndDispatch())
			{
				this.getDisplay().sleep();
				if ( ! this.getMaximized())
				{
					this.setMaximized(true);
				}
			}
			
		}
		this.dispose();


	}
*/	/**
	 * @param args
	 */
	/*public static void main(String[] args) {
		// TODO Auto-generated method stub
		//display = Display.getDefault();
	//	TimePass tp = new TimePass(this,display);
			}
*/
	private void makeToolBar()
	{
		ToolBar tb = new ToolBar(this ,SWT.BORDER| SWT.VERTICAL);
		Color clrBlack = Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
		Color clrWhite = Display.getCurrent().getSystemColor(SWT.COLOR_WHITE);
		tb.setBackground(clrBlack);
		tb.setForeground(clrWhite);
		
		ToolItem tiAccount = new ToolItem(tb , SWT.PUSH );
		tiAccount.setText("add account");
		ToolItem buffer = new ToolItem(tb, SWT.SEPARATOR);
		ToolItem tiVoucher = new ToolItem(tb, SWT.CASCADE);
		tiVoucher.setText("vouchers");
		tb.pack();
		

	}
}
