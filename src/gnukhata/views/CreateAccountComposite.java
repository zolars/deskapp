package gnukhata.views;

import java.text.NumberFormat;

import javax.swing.JList.DropLocation;

import gnukhata.globals;
import gnukhata.controllers.accountController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.internal.SWTEventListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;



public class CreateAccountComposite extends Composite
{
	Color Background;
	Color Foreground;
	Color FocusBackground;
	
	Color FocusForeground;
	Color BtnFocusForeground;
	boolean onceEdited = false;
	static Display display;
	TabFolder tfAccount;
	TabItem tiAddNewAccount;
	public static Label lblsavemsg;
Label lblGroupName;
Label lblcrdrblnc;
CCombo dropdownGroupName;
Label lblSubGroupName;
CCombo dropdownSubGroupName;
Label lblAccountName;
Text txtAccountName;
Label lblOpeningBalance; 
Text txtOpeningBalance;
Label lblTotalDrOpeningBalance;
Label txtTotalDrOpeningBalance ;
Label lblTotalCrOpeningBalance;
Label txtTotalCrOpeningBalance;
Label lblDiffInOpeningBalance;
Label txtDiffInOpeningBalance;
CCombo dropdownGroupName2;
Button btnSearch;
Button btnEdit;
Button btnConfirm;
Button btnDelete;
Button btnaddmultipleaccounts;
TabItem tiEditAccount;
Label lblAccountCode;
Text txtAccountCode;
Label lblnewSubGroupName;
Text  txtnewSubGroupName;
Button btnSave;
Button btnReset;
String searchText = "";
Label txtgroup;
Text txtaccountname;
String grpname;
String subgroup;
int getGroupIndex;
int getSubGroupIndex;
boolean multiaccounts;
boolean verifyFlag = false; 
long searchTexttimeout = 0;
	String suggestedAccountCode = "null";
	Object[] queryParams = new Object[8];
	CreateAccountComposite cac;
	FindAndEditAccountComposite fec;
	NumberFormat nf;
	boolean accountExists = false;
	public CreateAccountComposite(Composite parent, int style) 
	{
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
	    this.setLayout(new FormLayout());
	   MainShell.lblLogo.setVisible(false);
	   MainShell.lblLine.setVisible(false);
	   MainShell.lblOrgDetails.setVisible(false);

		lblsavemsg = new Label(this, SWT.NONE);
		lblsavemsg.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD|SWT.COLOR_RED));
		FormData layout = new FormData();
		layout.top = new FormAttachment(10);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(93);
		lblsavemsg.setLayoutData(layout);
	
		
	    lblGroupName = new Label(this, SWT.NONE);
	    lblGroupName.setText("Grou&p Name :");
		lblGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(16);
		layout.left = new FormAttachment(2);
		/*layout.right = new FormAttachment(22);
		layout.bottom = new FormAttachment(21);
		*/lblGroupName.setLayoutData(layout);
		

		dropdownGroupName = new CCombo(this, SWT.READ_ONLY | SWT.BORDER);
		dropdownGroupName.setFont(new Font(display,"Times New Romen",9,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(16);
		layout.left = new FormAttachment(24);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(21);
		dropdownGroupName.setLayoutData(layout);
		
		lblSubGroupName = new Label(this, SWT.NONE);
		lblSubGroupName.setText("S&ub-Group Name :");
		lblSubGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(2);
		/*layout.right = new FormAttachment(23);
		layout.bottom = new FormAttachment(29);
		*/lblSubGroupName.setLayoutData(layout);
		
		dropdownSubGroupName = new CCombo(this, SWT.READ_ONLY| SWT.BORDER);
		dropdownSubGroupName.setFont(new Font(display,"Times New Romen",9,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(24);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(29);
		dropdownSubGroupName.setLayoutData(layout);
		dropdownSubGroupName.add("------------Please select-----------");
		dropdownSubGroupName.select(0);
		
		dropdownSubGroupName.setEnabled(false);
		
		/*btnaddmultipleaccounts = new Button(this, SWT.NONE);
		btnaddmultipleaccounts.setText("Create Multiple Accounts");
		btnaddmultipleaccounts.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(41);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(47);
		layout.bottom = new FormAttachment(46);
		btnaddmultipleaccounts.setLayoutData(layout);
		btnaddmultipleaccounts.setEnabled(false);
		*/
		lblnewSubGroupName = new Label(this, SWT.NONE);
		lblnewSubGroupName.setText("New Su&b-Group Name:");
		lblnewSubGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(33);
		layout.left = new FormAttachment(2);
		/*layout.right = new FormAttachment(23);
		layout.bottom = new FormAttachment(38);
		*/lblnewSubGroupName.setLayoutData(layout);
		lblnewSubGroupName.setVisible(false);
		
		txtnewSubGroupName = new Text(this, SWT.BORDER);
		txtnewSubGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(33);
		layout.left = new FormAttachment(24);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(38);
		txtnewSubGroupName.setLayoutData(layout);
		
		txtnewSubGroupName.setVisible(false);
		
		btnaddmultipleaccounts = new Button(this, SWT.CHECK);
		btnaddmultipleaccounts.setText("Create Multiple Accounts");
		btnaddmultipleaccounts.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		btnaddmultipleaccounts.setToolTipText("Please Select the Group Name");
		layout = new FormData();
		layout.top = new FormAttachment(41);
		layout.left = new FormAttachment(24);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(46);
		btnaddmultipleaccounts.setLayoutData(layout);
		btnaddmultipleaccounts.setEnabled(false);
	
		lblAccountName = new Label(this, SWT.NONE);
		lblAccountName.setText("A&ccount Name :");
		lblAccountName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(56);
		layout.left = new FormAttachment(2);
		/*layout.right = new FormAttachment(20);
		layout.bottom = new FormAttachment(56);
		*/lblAccountName.setLayoutData(layout);
		
		txtAccountName = new Text(this, SWT.BORDER);
		txtAccountName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(56);
		layout.left = new FormAttachment(24);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(55);
		txtAccountName.setLayoutData(layout);
		
		lblAccountCode = new Label(this, SWT.NONE);
		lblAccountCode.setText("Account Code :");
		lblAccountCode.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		/*layout.top = new FormAttachment(56);
		layout.left = new FormAttachment(2);
		*//*layout.right = new FormAttachment(26);
		layout.bottom = new FormAttachment(56);
		*/lblAccountCode.setLayoutData(layout);
		lblAccountCode.setVisible(false);
		
		txtAccountCode = new Text(this, SWT.BORDER);
		txtAccountCode.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		/*layout.top = new FormAttachment(56);
		layout.left = new FormAttachment(27);
		*//*layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(56);
		*/txtAccountCode.setLayoutData(layout);
		txtAccountCode.setVisible(false);
		
		lblOpeningBalance = new Label(this, SWT.NONE);
		lblOpeningBalance.setText("&Opening Balance :");
		lblOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(65);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(22);
		//layout.bottom = new FormAttachment(65);
		lblOpeningBalance.setLayoutData(layout);
		
		txtOpeningBalance = new Text(this, SWT.RIGHT|SWT.BORDER);
		//txtOpeningBalance.setMessage("0.00");
		txtOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(65);
		layout.left = new FormAttachment(24);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(66);
		txtOpeningBalance.setText("0.00");
		txtOpeningBalance.setToolTipText("Enter opening balance(optional)");
		txtOpeningBalance.setLayoutData(layout);
		
		lblTotalDrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalDrOpeningBalance.setText("Total Debit Opening Balance:");
		lblTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(16);
		layout.left = new FormAttachment(54);
		/*layout.right = new FormAttachment(100);
		layout.bottom = new FormAttachment(25);
		*/lblTotalDrOpeningBalance.setLayoutData(layout);
		
		
		txtTotalDrOpeningBalance = new Label(this, SWT.RIGHT|SWT.READ_ONLY|SWT.BORDER);
		
		nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		nf.setGroupingUsed(false);
		
		txtTotalDrOpeningBalance.setText(nf.format( accountController.getTotalDr()));
		txtTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(16);
		layout.left = new FormAttachment(78);
		layout.right = new FormAttachment(90);
		//layout.bottom = new FormAttachment(35);
		txtTotalDrOpeningBalance.setLayoutData(layout);
		
		/*lblTotalDrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalDrOpeningBalance.setText("aaa");
		lblTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(28);
		layout.left = new FormAttachment(89);
		layout.right = new FormAttachment(93);
		layout.bottom = new FormAttachment(35);
		lblTotalDrOpeningBalance.setLayoutData(layout);
		*/
		lblTotalCrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalCrOpeningBalance.setText("Total Credit Opening Balance :");
		lblTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(40);
		layout.left = new FormAttachment(54);
		/*layout.right = new FormAttachment(100);
		layout.bottom = new FormAttachment(45);
		*/lblTotalCrOpeningBalance.setLayoutData(layout);
		
		
		txtTotalCrOpeningBalance = new Label(this, SWT.RIGHT|SWT.READ_ONLY|SWT.BORDER);
		txtTotalCrOpeningBalance.setText(nf.format(accountController.getTotalCr() ) );
		txtTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(40);
		layout.left = new FormAttachment(78);
		layout.right = new FormAttachment(90);
		//layout.bottom = new FormAttachment(57);
		txtTotalCrOpeningBalance.setLayoutData(layout);
		
		/*lblTotalCrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalCrOpeningBalance.setText(" ");
		lblTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(51);
		layout.left = new FormAttachment(89);
		layout.right = new FormAttachment(93);
		layout.bottom = new FormAttachment(60);
		lblTotalCrOpeningBalance.setLayoutData(layout);
		*/
		
		lblDiffInOpeningBalance = new Label(this, SWT.NONE);
		lblDiffInOpeningBalance.setText("Difference in Opening Balance :");
		lblDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(60);
		layout.left = new FormAttachment(54);
		/*layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(65);
		*/lblDiffInOpeningBalance.setLayoutData(layout);
		
		
		lblcrdrblnc = new Label(this, SWT.NONE);
		lblcrdrblnc.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		double balDiff = accountController.getTotalDr() - accountController.getTotalCr();
		Double val=balDiff;
		if(val>0)
		{
			lblcrdrblnc.setText("Dr");
		}
		else if (val<0) 
		{
			lblcrdrblnc.setText("Cr");
		}
		else
		{
			lblcrdrblnc.setText("");
		}
		layout = new FormData();
		layout.top = new FormAttachment(60);
		layout.left = new FormAttachment(92);
		/*layout.right = new FormAttachment(76);
		layout.bottom = new FormAttachment(76);
		*/lblcrdrblnc.setLayoutData(layout);
		
		txtDiffInOpeningBalance = new Label(this, SWT.RIGHT|SWT.BORDER|SWT.READ_ONLY);
		if(val < 0 )
		{
			val = Math.abs(val);
		}
		txtDiffInOpeningBalance.setText(nf.format(Double.parseDouble(val.toString())));
		txtDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(60);
		layout.left = new FormAttachment(78);
		layout.right = new FormAttachment(90);
		//layout.bottom = new FormAttachment(77);
		txtDiffInOpeningBalance.setLayoutData(layout);
		//txtDiffInOpeningBalance.setText(String.valueOf(Double.valueOf(txtTotalDrOpeningBalance)) - (Double.valueOf(txtTotalCrOpeningBalance)));
		
		/*lblDiffInOpeningBalance = new Label(this, SWT.NONE);
		lblDiffInOpeningBalance.setText("");
		lblDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(70);
		layout.left = new FormAttachment(89);
		layout.right = new FormAttachment(93);
		layout.bottom = new FormAttachment(77);
		lblDiffInOpeningBalance.setLayoutData(layout);
		
		*/
		btnSave = new Button(this, SWT.NONE);
		btnSave.setText("&Save");
		btnSave.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(90);
		layout.left = new FormAttachment(25);
		layout.right = new FormAttachment(33);
		layout.bottom = new FormAttachment(96);
		btnSave.setToolTipText("Click here to save current entry changes");
		btnSave.setLayoutData(layout);
		btnSave.setEnabled(false);
				
		btnReset = new Button(this, SWT.NONE);
		btnReset.setText("R&eset");
		btnReset.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(90);
		layout.left = new FormAttachment(40);
		layout.right = new FormAttachment(48);
		layout.bottom = new FormAttachment(96);
		btnReset.setToolTipText("Click here to cancel current entry ");
		btnReset.setLayoutData(layout);
		
		String[] allgroups = gnukhata.controllers.accountController.getAllGroups();
		dropdownGroupName.add("------------Please select-----------");
		dropdownGroupName.select(0);
		
		for (int i = 0; i < allgroups.length; i++ )
		{
			dropdownGroupName.add(allgroups[i]);
			
		}
		//dropdownGroupName.setListVisible(true);
		if(globals.session[5].toString().equals("automatic") )
		{
			txtAccountCode.setEnabled(false);
		}
		
		this.getAccessible();
		//this.makeToolBar();
		this.setEvents();
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);

		globals.setThemeColor(this, Background, Foreground);
        globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        this.pack();
	    this.makeaccssible(this);
	    dropdownGroupName.setFocus();
	    dropdownGroupName.setBackground(FocusBackground);
	    dropdownGroupName.setForeground(FocusForeground);
	


	   	
	}
	
	private void setEvents()
	{		
		this.txtOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				verifyFlag=true;
				if(dropdownGroupName.getSelectionIndex()== 0 || dropdownSubGroupName.getSelectionIndex()==0||(txtnewSubGroupName.getVisible()&&txtnewSubGroupName.getText().trim().equals("")) || txtAccountName.getText().trim().equals(""))
				{
					btnSave.setEnabled(false);
				}
				else
				{
					btnSave.setEnabled(true);
				}
					
			}
			public void focusLost(FocusEvent arg0){
				verifyFlag=false;

				try {
					txtOpeningBalance.setText(nf.format(Double.parseDouble(txtOpeningBalance.getText())));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					txtOpeningBalance.setText("");
				}
			}
		});
	
		dropdownGroupName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				//dropdownGroupName.setListVisible(true);
				//btnSave.setEnabled(false);
				if(dropdownGroupName.getSelectionIndex()>0)
				{
					
				dropdownSubGroupName.setEnabled(true);
				String selectedGroup = dropdownGroupName.getItem(dropdownGroupName.getSelectionIndex());
				String[] subGroups = gnukhata.controllers.accountController.getSubGroups(selectedGroup);
				
				/*String[] accountlist=gnukhata.controllers.accountController.getAccountByGroup(selectedGroup);
				for (int i = 0; i < accountlist.length; i++) 
				{
					MessageBox msg=new MessageBox(new Shell(),SWT.OK);
					msg.setMessage(accountlist[i]);
					msg.open();
				}*/
				
				
				queryParams[0] = selectedGroup;
			//	dropdownSubGroupName.removeAll();
			/*	dropdownSubGroupName.add("------------Please select-----------");
				dropdownSubGroupName.select(0);
			*/	if(dropdownGroupName.getText().trim().equals("Please select"))
				{
					lblOpeningBalance.setText("&Opening Balance :");
				}
				if(selectedGroup.equals("Current Assets") || selectedGroup.equals("Fixed Assets") || selectedGroup.equals("Investment") || selectedGroup.equals("Loans(Asset)"))
				{
					lblOpeningBalance.setText("Debit &Opening Balance");
				}
				if (selectedGroup.equals("Capital") || selectedGroup.equals("Corpus") || selectedGroup.equals("Current Liability") || selectedGroup.equals("Loans(Liability)") || selectedGroup.equals("Miscellaneous Expenses(Asset)") || selectedGroup.equals("Reserves") )
				{
					lblOpeningBalance.setText("Credit &Opening Balance");
				}
				if( selectedGroup.equals("Direct Income") || selectedGroup.equals("Indirect Income") || selectedGroup.equals("Direct Expense") || selectedGroup.equals("Indirect Expense") )
				{
					lblOpeningBalance.setEnabled(false);
					
					txtOpeningBalance.setEnabled(false);
					txtTotalCrOpeningBalance.setVisible(false);
					txtTotalDrOpeningBalance.setVisible(false);
					lblTotalCrOpeningBalance.setVisible(false);
					lblTotalDrOpeningBalance.setVisible(false);
					lblDiffInOpeningBalance.setVisible(false);
					txtDiffInOpeningBalance.setVisible(false);
					lblcrdrblnc.setVisible(false);
				
					//dropdownSubGroupName.add("No Sub-Group");
				}
				else
				{
					lblOpeningBalance.setEnabled(true);
					txtOpeningBalance.setEnabled(true);
					txtTotalCrOpeningBalance.setVisible(true);
					txtTotalDrOpeningBalance.setVisible(true);
					lblTotalCrOpeningBalance.setVisible(true);
					lblTotalDrOpeningBalance.setVisible(true);
					lblDiffInOpeningBalance.setVisible(true);
					txtDiffInOpeningBalance.setVisible(true);
					lblcrdrblnc.setVisible(true);
				
				}
				
				/*else
				{
					txtOpeningBalance.setEnabled(false);
				}*/
												
			}
				else
				{
					
					dropdownSubGroupName.select(0);
					dropdownSubGroupName.setEnabled(false);
				}
				
				
				if(dropdownGroupName.getSelectionIndex()== 0 || dropdownSubGroupName.getSelectionIndex()==0||(txtnewSubGroupName.getVisible()&&txtnewSubGroupName.getText().trim().equals("")) || txtAccountName.getText().trim().equals(""))
				{
					btnSave.setEnabled(false);
				}
				else
				{
					btnSave.setEnabled(true);
				}
				if(! lblsavemsg.getText().equals(""))
				{
					Display.getCurrent().asyncExec(new Runnable(){
						public void run()
						{
							long now = System.currentTimeMillis();
							long lblTimeOUt = 0;
							while(lblTimeOUt < (now + 2000))
							{
								lblTimeOUt = System.currentTimeMillis();
							}
					
							lblsavemsg.setText("");

						}
				});

					
				}
				//dropdownGroupName.setFocus();
			}
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
			//	super.focusLost(arg0);
			if(dropdownGroupName.getSelectionIndex()== 0)
			{
				MessageBox msgVoucherCode = new MessageBox(new Shell(),SWT.OK);
				msgVoucherCode.setText("Error!");
				msgVoucherCode.setMessage("Please select Group Name");
				//msgVoucherCode.open();
				return;
			
			}
			else
			{
				dropdownSubGroupName.setFocus();
				return;
			}
			}
		});

		
		txtTotalCrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				btnSave.setFocus();
			}
		});
		txtTotalDrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				btnSave.setFocus();
			}
		});
		txtDiffInOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				btnSave.setFocus(); 
			}
		});
		
		this.dropdownGroupName.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				grpname=dropdownGroupName.getText();
				
				/*if(dropdownGroupName.getSelectionIndex() > 0 )
				{
					btnaddmultipleaccounts.setEnabled(true);
					
				MessageBox msgVoucherCode = new MessageBox(new Shell(),SWT.OK);
				msgVoucherCode.setText("Error!");
				msgVoucherCode.setMessage("Please enter Group For account");
				msgVoucherCode.open();
				return;
				
				
					return;
					}*/
				
			
				if(dropdownGroupName.getSelectionIndex()>0)
				{
					
				dropdownSubGroupName.setEnabled(true);
				String selectedGroup = dropdownGroupName.getItem(dropdownGroupName.getSelectionIndex());
				String[] subGroups = gnukhata.controllers.accountController.getSubGroups(selectedGroup);
				
				/*String[] accountlist=gnukhata.controllers.accountController.getAccountByGroup(selectedGroup);
				for (int i = 0; i < accountlist.length; i++) 
				{
					MessageBox msg=new MessageBox(new Shell(),SWT.OK);
					msg.setMessage(accountlist[i]);
					msg.open();
				}*/
				
				
				queryParams[0] = selectedGroup;
				dropdownSubGroupName.removeAll();
				dropdownSubGroupName.add("------------Please select-----------");
				dropdownSubGroupName.select(0);
				
				
				if(dropdownGroupName.getText().trim().equals("Please select"))
				{
					lblOpeningBalance.setText("&Opening Balance :");
				}
				if(selectedGroup.equals("Current Assets") || selectedGroup.equals("Fixed Assets") || selectedGroup.equals("Investment") || selectedGroup.equals("Loans(Asset)"))
				{
					lblOpeningBalance.setText("Debit &Opening Balance");
				}
				if (selectedGroup.equals("Capital") || selectedGroup.equals("Corpus") || selectedGroup.equals("Current Liability") || selectedGroup.equals("Loans(Liability)") || selectedGroup.equals("Miscellaneous Expenses(Asset)") || selectedGroup.equals("Reserves") )
				{
					lblOpeningBalance.setText("Credit &Opening Balance");
				}
				if( selectedGroup.equals("Direct Income") || selectedGroup.equals("Indirect Income") || selectedGroup.equals("Direct Expense") || selectedGroup.equals("Indirect Expense") )
				{
					lblOpeningBalance.setEnabled(false);
					
					txtOpeningBalance.setEnabled(false);
					txtTotalCrOpeningBalance.setVisible(false);
					txtTotalDrOpeningBalance.setVisible(false);
					lblTotalCrOpeningBalance.setVisible(false);
					lblTotalDrOpeningBalance.setVisible(false);
					lblDiffInOpeningBalance.setVisible(false);
					txtDiffInOpeningBalance.setVisible(false);
					lblcrdrblnc.setVisible(false);
				
					
				}
				else
				{
					lblOpeningBalance.setEnabled(true);
					txtOpeningBalance.setEnabled(true);
					txtTotalCrOpeningBalance.setVisible(true);
					txtTotalDrOpeningBalance.setVisible(true);
					lblTotalCrOpeningBalance.setVisible(true);
					lblTotalDrOpeningBalance.setVisible(true);
					lblDiffInOpeningBalance.setVisible(true);
					txtDiffInOpeningBalance.setVisible(true);
					lblcrdrblnc.setVisible(true);
				
				}
				
				/*else
				{
					txtOpeningBalance.setEnabled(false);
				}*/
				for (int i = 0; i < subGroups.length; i++ )
					dropdownSubGroupName.add(subGroups[i]);
				
				//Generating Account code
				if(selectedGroup.equals("Capital"))
				{
					suggestedAccountCode = "CP";
				}
				if(selectedGroup.equals("Current Assets"))
				{
					suggestedAccountCode = "CA";
				}
				if(selectedGroup.equals("Fixed Assets"))
				{
					suggestedAccountCode = "FA";
				}
				if(selectedGroup.equals("Direct Income"))
				{
					suggestedAccountCode = "DI";
				}
				if(selectedGroup.equals("Indirect Income"))
				{
					suggestedAccountCode = "II";
				}
				if(selectedGroup.equals("Current Liability"))
				{
					suggestedAccountCode = "CL";
				}
				if(selectedGroup.equals("Loans(Asset)"))
				{
					suggestedAccountCode = "LA";
				}
				if(selectedGroup.equals("Loans(Liability)"))
				{
					suggestedAccountCode = "LL";
				}
				if(selectedGroup.equals("Miscellaneous Expenses(Asset)"))
				{
					suggestedAccountCode = "ME";
				}
				if(selectedGroup.equals("Direct Expense"))
				{
					suggestedAccountCode = "DE";
				}
				if(selectedGroup.equals("Investment"))
				{
					suggestedAccountCode = "IV";
				}
				if(selectedGroup.equals("Reserves"))
				{
					suggestedAccountCode = "RS";
				}
				if(selectedGroup.equals("Indirect Expense"))
				{
					suggestedAccountCode = "IE";
				}								
			}
				else
				{
					
					dropdownSubGroupName.select(0);
					dropdownSubGroupName.setEnabled(false);
				}
			}
		});		
		
		dropdownSubGroupName.addFocusListener(new FocusAdapter() {
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				//btnSave.setEnabled(false);
				
					for (int i = 0; i < dropdownSubGroupName.getItemCount(); i++) {
						if (dropdownSubGroupName.getItem(i).trim().equals("No Sub-Group")) {
							dropdownSubGroupName.select(i);
							break;
						}
					}
					dropdownSubGroupName.notifyListeners(SWT.Selection, new Event());
				
				if(dropdownGroupName.getSelectionIndex() != 0)
				{
					btnaddmultipleaccounts.setEnabled(true);
					return;
				}
				else
				{
					btnaddmultipleaccounts.setEnabled(false);
					return;
				}
			}
		});
	
	btnaddmultipleaccounts.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			
			

			
			if(btnaddmultipleaccounts.getSelection())
			{
				multiaccounts = true;
				txtAccountName.setEnabled(false);
				txtOpeningBalance.setEnabled(false);
				btnSave.setEnabled(false);
				if(dropdownGroupName.getSelectionIndex() == 0)
				{
					Shell msgbox= new Shell();
					msgbox.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
					MessageBox	 msg = new MessageBox(msgbox,SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msg.setText("Error!");
					msg.setMessage("Please select Group name");
					msg.open();
					dropdownGroupName.setFocus();
					btnaddmultipleaccounts.setSelection(false);
					return;
				}
				
				if(dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex()).trim().equals("------------Please select-----------"))
					
				{
					Shell msgbox= new Shell();
					msgbox.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
					MessageBox	 msg = new MessageBox(msgbox,SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msg.setText("Error!");
					msg.setMessage("Please select Sub-Group name");
					msg.open();
					dropdownSubGroupName.setFocus();
					btnaddmultipleaccounts.setSelection(false);
					return;
					
				}
				if(txtnewSubGroupName.isVisible())
				{
					if(txtnewSubGroupName.getText().trim().equals(""))
					{
						Shell msgbox= new Shell();
						msgbox.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
						MessageBox	 msg = new MessageBox(msgbox,SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Please select Sub-Group name");
						msg.open();
						txtnewSubGroupName.setFocus();
						return;
					}
				}
			
				if(multiaccounts == true)
				{
					if(txtnewSubGroupName.isVisible())
					{
						subgroup = txtnewSubGroupName.getText().toString();
						
					}
					else
					{
						subgroup = dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex());
					}
				Shell popupShell=new Shell();
				AddMultipleAccounts amaDialog = new AddMultipleAccounts(popupShell, dropdownGroupName.getItem(dropdownGroupName.getSelectionIndex()),subgroup);
				amaDialog.open();
				
				if(AddMultipleAccounts.saveflag.equals(true))
				{
				
					dropdownGroupName.select(0);
					dropdownSubGroupName.select(0);
					lblnewSubGroupName.setVisible(false);
					txtnewSubGroupName.setVisible(false);
					btnaddmultipleaccounts.setSelection(false);
					btnaddmultipleaccounts.setEnabled(false);
					txtTotalCrOpeningBalance.setVisible(true);
					txtTotalDrOpeningBalance.setVisible(true);
					lblTotalCrOpeningBalance.setVisible(true);
					lblTotalDrOpeningBalance.setVisible(true);
					lblDiffInOpeningBalance.setVisible(true);
					txtDiffInOpeningBalance.setVisible(true);
					lblcrdrblnc.setVisible(true);
				
					dropdownGroupName.setFocus();
				}
				
				if(AddMultipleAccounts.cancelflag.equals(true))
				{
					popupShell.dispose();
					//btnReset.notifyListeners(SWT.Selection, new Event());
					
					if(txtnewSubGroupName.isVisible())
					{
						txtnewSubGroupName.setText(txtnewSubGroupName.getText());
						
					}
					else
					{
						subgroup = dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex());
						dropdownSubGroupName.setText(subgroup);
					}

					btnaddmultipleaccounts.setSelection(false);
					dropdownGroupName.setFocus();
					
					//dropdownGroupName.select(0);
					//dropdownSubGroupName.select(0);
					txtAccountName.setText("");
					
					txtAccountName.setEnabled(true);
					lblAccountName.setEnabled(true);
					txtOpeningBalance.setEnabled(true);
					btnSave.setEnabled(false);
					//btnaddmultipleaccounts.setEnabled(false);
		//			String selectedGroup = dropdownGroupName.getItem(dropdownGroupName.getSelectionIndex());
					/*if( selectedGroup.equals("Direct Income") || selectedGroup.equals("Indirect Income") || selectedGroup.equals("Direct Expense") || selectedGroup.equals("Indirect Expense") )
					{
						txtOpeningBalance.setEnabled(false);
						return;
					}
					else
					{
						txtOpeningBalance.setEnabled(true);
						return;
					}
					*/
					
					//txtOpeningBalance.setEnabled(true);
				}	
				}
			
			}
					
			else {
				multiaccounts = false;
				txtAccountName.setEnabled(true);
				txtAccountName.setFocus();
				txtOpeningBalance.setEnabled(true);
				btnSave.setEnabled(false);
				
				
			}
		}
	});
/*	btnaddmultipleaccounts.addFocusListener(new FocusAdapter() {
		
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
			if(dropdownGroupName.getSelectionIndex() == 0)
			{
				Shell msgbox= new Shell();
				msgbox.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
				MessageBox	 msg = new MessageBox(msgbox,SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
				msg.setText("Error!");
				msg.setMessage("Please select Group name");
				msg.open();
				dropdownGroupName.setFocus();
				return;
			}
			
			if(dropdownSubGroupName.getSelectionIndex() == 0)
				
			{
				Shell msgbox= new Shell();
				msgbox.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
				MessageBox	 msg = new MessageBox(msgbox,SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
				msg.setText("Error!");
				msg.setMessage("Please select Sub-Group name");
				msg.open();
				dropdownSubGroupName.setFocus();
				return;
				
			}
			if(txtnewSubGroupName.isVisible())
			{
				if(txtnewSubGroupName.getText().trim().equals(""))
				{
					Shell msgbox= new Shell();
					msgbox.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
					MessageBox	 msg = new MessageBox(msgbox,SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msg.setText("Error!");
					msg.setMessage("Please select Sub-Group name");
					msg.open();
					txtnewSubGroupName.setFocus();
					return;
				}
			}
			
		}
	});
*/	
	/*btnaddmultipleaccounts.addFocusListener(new FocusAdapter() {
		
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
			
			if(multiaccounts == true)
			{
			Shell popupShell=new Shell();
			AddMultipleAccounts amaDialog = new AddMultipleAccounts(popupShell, dropdownGroupName.getItem(dropdownGroupName.getSelectionIndex()), dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex()));
			amaDialog.open();
			
			if(AddMultipleAccounts.cancelflag.equals(true))
			{
				popupShell.dispose();
				dropdownGroupName.setFocus();
				return;
				
				
			}	
			}
					}
		
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			if(multiaccounts== true)
			{
				txtAccountName.clearSelection();
				txtAccountName.setEnabled(false);
				txtOpeningBalance.clearSelection();
				txtOpeningBalance.setEnabled(false);
				btnSave.setEnabled(false);
			}
			else
			{
				txtAccountName.setEnabled(true);
				txtOpeningBalance.setEnabled(true);
				btnSave.setEnabled(true);
			}
		}
	});
	*/
		
	/*	this.btnaddmultipleaccounts.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
			//	super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
			{
				
				
				txtAccountName.setFocus();
				return;
			}
			 if(arg0.keyCode == SWT.ARROW_UP)
			 {
				 if(txtnewSubGroupName.getVisible())
					{
						txtnewSubGroupName.setFocus();
					}
					else
					{
						dropdownSubGroupName.setFocus();
					}
				 }
			}
		});
	*/
	btnaddmultipleaccounts.addTraverseListener(new TraverseListener() {
		
		@Override
		public void keyTraversed(TraverseEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.detail == SWT.TRAVERSE_MNEMONIC)
			{
				arg0.doit = false;
				return;
			}
			if(arg0.detail == SWT.TRAVERSE_TAB_NEXT || arg0.detail == SWT.TRAVERSE_RETURN)
			{
				txtAccountName.setFocus();
				arg0.doit = false;
				return;
				
			}
			if(arg0.detail == SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail == SWT.TRAVERSE_ARROW_PREVIOUS)
			{
				 if(txtnewSubGroupName.getVisible())
					{
						txtnewSubGroupName.setFocus();
						//arg0.doit = false;
						return;
					}
					else
					{
						if(dropdownSubGroupName.isEnabled())
						{
							dropdownSubGroupName.setFocus();
						}
						else {
							dropdownGroupName.setFocus();
						}
					
					}
				 
			}
		}
	});
		dropdownGroupName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				//code here
				if(arg0.keyCode== SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
				{
                    
					if(dropdownGroupName.getSelectionIndex() == 0)
					{
						Shell msgbox= new Shell();
						msgbox.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
						MessageBox	 msg = new MessageBox(msgbox,SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Please select Group name");
						msg.open();
						dropdownGroupName.setFocus();
						return;
						
					}
					else if(dropdownGroupName.getSelectionIndex() != 0)
					{
					dropdownGroupName.notifyListeners(SWT.Selection, new Event());
					dropdownSubGroupName.setFocus();
					}
					return;
				}
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchTexttimeout = now + 500;
				
				for(int i = 0; i < dropdownGroupName.getItemCount(); i++ )
				{
					if(dropdownGroupName.getItem(i).toLowerCase().startsWith(searchText ) ){
						dropdownGroupName.select(i);
						dropdownGroupName.notifyListeners(SWT.Selection,new Event());
						break;
					}
				}
			
			}
		});
		
		this.dropdownSubGroupName.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				//subgrpname=dropdownSubGroupName.getText();
				if(dropdownGroupName.getSelectionIndex()== 0 || dropdownSubGroupName.getSelectionIndex()==0||(txtnewSubGroupName.getVisible()&&txtnewSubGroupName.getText().trim().equals("")) || txtAccountName.getText().trim().equals(""))
				{
					btnSave.setEnabled(false);
				}
				else
				{
					btnSave.setEnabled(true);
				}
				
				if(dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex()).equals("Create New Sub-Group"))
				{
					lblnewSubGroupName.setVisible(true);
					txtnewSubGroupName.setVisible(true);
					queryParams[1] = "Create New Sub-Group";
					
				}
				else
				{
					lblnewSubGroupName.setVisible(false);
					txtnewSubGroupName.setVisible(false);
					queryParams[1] = dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex());
					queryParams[2] = ""; 

				}
			}
			
		});
		dropdownSubGroupName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchTexttimeout = now + 500;
				
				for(int i = 0; i < dropdownSubGroupName.getItemCount(); i++ )
				{
					if(dropdownSubGroupName.getItem(i).toLowerCase().startsWith(searchText ) ){
						dropdownSubGroupName.select(i);
						dropdownSubGroupName.notifyListeners(SWT.Selection,new Event());
						break;
					}
				}
			
			}
		});
		
		this.dropdownSubGroupName.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				
				if(arg0.detail == SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit = false;
					return;
					
				}
				if(arg0.detail == SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail == SWT.TRAVERSE_ARROW_PREVIOUS)
				{
					if(dropdownSubGroupName.getSelectionIndex() == 0)
					{
						dropdownGroupName.setFocus();
						btnaddmultipleaccounts.setEnabled(false);
					}
				
				//arg0.doit = false;
				
				return;
				}
				if(arg0.detail == SWT.TRAVERSE_TAB_NEXT || arg0.detail == SWT.TRAVERSE_RETURN)
				{
					
					/*if(dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex()).trim().equals("------------Please select-----------"))
					{
						MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Please select Sub-Group name");
						dropdownSubGroupName.setFocus();
						msg.open();
						arg0.doit = false;
						return;
						//arg0.doit=false;
					}
					*/if(dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex()).trim().equals("Create New Sub-Group"))
					{
						txtnewSubGroupName.setVisible(true);
						txtnewSubGroupName.setFocus();
						arg0.doit = false;
						return;
					}
					else
					{
						btnaddmultipleaccounts.setFocus();
						arg0.doit = false;
						return;
					}
				}
				
			}
		});
		this.txtnewSubGroupName.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				
				if(arg0.detail == SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit = false;
					return;
					
				}
				
				if(arg0.detail == SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail == SWT.TRAVERSE_ARROW_PREVIOUS)
				{
					dropdownSubGroupName.setFocus();
					return;
				}
				if(arg0.detail == SWT.TRAVERSE_TAB_NEXT || arg0.detail == SWT.TRAVERSE_RETURN)
				{
					if(txtnewSubGroupName.getText().trim().equals(""))
					{
						MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Please Select Sub-Group name");
						
						msg.open();
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtnewSubGroupName.setFocus();
							}
						});
						arg0.doit = false;
						return;
						
					}
					else
					{
						btnaddmultipleaccounts.setFocus();
						arg0.doit = false;
						return;
						
					}
				
					}
			}
		});
		/*this.dropdownSubGroupName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					if(dropdownSubGroupName.getSelectionIndex()< 0)
					{
						MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Please select Subgroup name");
						msg.open();
						dropdownSubGroupName.setFocus();
						
					}
					else if(dropdownSubGroupName.getSelectionIndex() >= 0)
					{
						if(txtnewSubGroupName.isVisible())
						{
							txtnewSubGroupName.setFocus();		
						}
						else
						{
							if(multiaccounts ==false)
							{
							txtAccountName.setFocus();
							}
						}
					}
				}
				if(e.keyCode== SWT.ARROW_UP )
				{
					if(dropdownSubGroupName.getSelectionIndex() == 0)
					{
						dropdownGroupName.setFocus();
					}
				}
				
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(e.character);
				searchTexttimeout = now + 1000;					
				for(int i = 0; i < dropdownSubGroupName.getItemCount(); i++ )
				{
					if(dropdownSubGroupName.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropdownSubGroupName.select(i);
						break;
					}
				}				
			}
		});
		
		*/
		
		/*this.txtnewSubGroupName.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
			{
				if(!txtnewSubGroupName.getText().trim().equals(""))
				{
					String subgrpname1= txtnewSubGroupName.getText();
					String titlecasename= toTitleCase(subgrpname1);
					txtnewSubGroupName.setText(titlecasename);
				}
				String result = accountController.subgroupExists(txtnewSubGroupName.getText());
				MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
				msg.setText("Error!");
				if( Integer.valueOf(result) == 1)
				{
					
					msg.setMessage("The subgroup name you entered already exists");
					msg.open();
					txtnewSubGroupName.setText("");
					txtnewSubGroupName.setFocus();
				}

				
				
				if(txtnewSubGroupName.isEnabled() && multiaccounts ==false)
				{
				txtAccountName.setFocus();
				}
				else
				{
					if(e.keyCode== SWT.ARROW_UP)
					{
						if(txtnewSubGroupName.getVisible())
						{
							txtnewSubGroupName.setFocus();
						}
						else
						{
							dropdownSubGroupName.setFocus();
						}
						
					}
				
				}
			}
			if(e.keyCode==SWT.ARROW_UP)
			{
				dropdownSubGroupName.setFocus();					
			}		
		}			
		});
		this.txtnewSubGroupName.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) 
		{	
						
		}
		});
*/		
		this.txtnewSubGroupName.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				if(e.keyCode==8||e.keyCode==46)
				{
					if(txtnewSubGroupName.getText().trim().equals(""))
					{
						btnSave.setEnabled(false);
					}
				}
				if((e.keyCode >=48 && e.keyCode <=57) || (e.keyCode >=97 && e.keyCode <=122) || (e.keyCode >=65 && e.keyCode<=90))
				{
					if(dropdownGroupName.getSelectionIndex()== 0 || dropdownSubGroupName.getSelectionIndex()==0||(txtnewSubGroupName.getVisible()&&txtnewSubGroupName.getText().trim().equals("")) || txtAccountName.getText().trim().equals(""))
					{
						btnSave.setEnabled(false);
					}
					else
					{
						btnSave.setEnabled(true);
					}
					
				}
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		txtnewSubGroupName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				//btnSave.setEnabled(false);
				if(dropdownGroupName.getSelectionIndex()== 0 || dropdownSubGroupName.getSelectionIndex()==0||(txtnewSubGroupName.getVisible()&&txtnewSubGroupName.getText().trim().equals("")) || txtAccountName.getText().trim().equals(""))
				{
					btnSave.setEnabled(false);
				}
				else
				{
					btnSave.setEnabled(true);
				}
			}
		});
		this.txtAccountName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				
					
				}
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				
					if(dropdownGroupName.getSelectionIndex()== 0 || dropdownSubGroupName.getText().trim().equals("------------Please select-----------")||dropdownSubGroupName.getText().equals("Create New Sub-Group") && txtnewSubGroupName.getText().trim().equals("")||txtAccountName.getText().trim().equals(""))
					{
						btnSave.setEnabled(false);
					}
					else
					{
						btnSave.setEnabled(true);
					}
					
					
				
		
			}
			
		/*	@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(dropdownGroupName.getSelectionIndex() == 0)
				{
					MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
					errMessage.setText("Error!");
					errMessage.setMessage("Please select a group name");
					errMessage.open();
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							dropdownGroupName.setFocus();
						}
					});
					
					return;
				}
			}*/
		});
		this.txtAccountName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					if(txtOpeningBalance.isEnabled())
					{
						txtOpeningBalance.setFocus();
					}
					else
					{
						btnSave.notifyListeners(SWT.Selection, new Event());
						
					
					}
				}
				if(e.keyCode== SWT.ARROW_UP)
				{
					btnaddmultipleaccounts.setFocus();
					return;
				}
				
			}
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyReleased(arg0);
				if(e.keyCode==8||e.keyCode==46)
				{
					if(txtAccountName.getText().trim().equals(""))
					{
						btnSave.setEnabled(false);
					}
				}
				if((e.keyCode >=48 && e.keyCode <=57) || (e.keyCode >=97 && e.keyCode <=122) || (e.keyCode >=65 && e.keyCode<=90))
				{
					if(dropdownGroupName.getSelectionIndex()== 0 || dropdownSubGroupName.getSelectionIndex()==0||(txtnewSubGroupName.getVisible()&&txtnewSubGroupName.getText().trim().equals("")) || txtAccountName.getText().trim().equals(""))
					{
						btnSave.setEnabled(false);
					}
					else
					{
						btnSave.setEnabled(true);
					}
					
				}
			}
		});
		this.txtOpeningBalance.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					btnSave.notifyListeners(SWT.Selection, new Event());
				}
				if(e.keyCode == SWT.ARROW_UP)
				{
					
					txtAccountName.setFocus();
				}
			}
		});
		this.btnSave.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			
			queryParams[3] = txtAccountName.getText();
			queryParams[4] = globals.session[5];
			if(globals.session[5].toString().equals("automatic"))
			{
				queryParams[7] = "";
			}
			else
			{
				queryParams[7] = txtAccountCode.getText();
			}
			//queryParams[7] = suggestedAccountCode;
			//super.widgetSelected(arg0);
			String result = accountController.accountExists(txtAccountName.getText());
			if (Integer.valueOf(result) == 1)
			{
				MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
				msg.setText("Warning!");
				msg.setMessage("The account name you entered already exists, please choose another name.");
				msg.open();
				
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtAccountName.setText("");
						txtAccountName.setFocus();
						
					}
				});
				return;
			}
			if(dropdownGroupName.getSelectionIndex()== 0 )
			{
				MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
				errMessage.setText("Error!");
				errMessage.setMessage("Please select a group for this account");
				errMessage.open();
				dropdownGroupName.setFocus();
				return;
			}
			if(dropdownSubGroupName.getText().trim().equals("------------Please select-----------"))
			{
				MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
				errMessage.setText("Error!");
				errMessage.setMessage("Please select a Sub-group or NO Sub-Group for this account");
				errMessage.open();
				dropdownSubGroupName.setFocus();
				return;
			}
			if ( dropdownSubGroupName.getText().equals("Create New Sub-Group") && txtnewSubGroupName.getText().trim().equals(""))
			{
				MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
				msg.setText("Error");
				msg.setMessage("Please enter an New Sub-Group Name");
				msg.open();
				txtnewSubGroupName.setFocus();
				return;
			}
			
			if(txtAccountName.getText().trim().equals(""))
			{
				
				Shell msgbox=new Shell();
				msgbox.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
				MessageBox errMessage = new MessageBox(msgbox,SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
				errMessage.setText("Error!");
				errMessage.setMessage("Please enter an Account Name");
				errMessage.open();
				txtAccountName.setFocus();

				return;
				}
				
			
			
			if(txtnewSubGroupName.getVisible())
			{
				queryParams[2] = txtnewSubGroupName.getText();
				
			} 
			if(txtOpeningBalance.getText().trim().equals("")|| txtOpeningBalance.getEnabled()== false )
			{
				queryParams[5] = "0.00";
				queryParams[6] = "0.00";
				
			}
			else
			{
				NumberFormat nf = NumberFormat.getInstance();
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				nf.setGroupingUsed(false);

				try {
					queryParams[5] = nf.format(Double.valueOf(txtOpeningBalance.getText()));
					queryParams[6] = nf.format(Double.valueOf(txtOpeningBalance.getText()));
				
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					MessageBox msgerr = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
					msgerr.setText("Invalid Amount");
					msgerr.setMessage("You have entered an invalid amount");
					msgerr.open();
					
					return;
				}

					}
			
			if(accountController.setAccount(queryParams))
			{
				/*MessageBox successMsg = new MessageBox(new Shell(),SWT.OK| SWT.ICON_INFORMATION);
				successMsg.setText("success");
				successMsg.setMessage("Account "+ txtAccountName.getText() + " added successfully" );
				successMsg.open();
				*/
				lblsavemsg.setText("Account  " + txtAccountName.getText().replace("&", "&&") + "  added successfully");
				lblsavemsg.setVisible(true);
				
				dropdownGroupName.select(0);
				if(dropdownGroupName.getSelectionIndex()==0)
				{
					dropdownSubGroupName.removeAll();
					dropdownSubGroupName.add("------------Please select-----------");
				}
				dropdownSubGroupName.select(0);
				txtAccountCode.setText("");
				txtAccountName.setText("");
				txtOpeningBalance.setText("");
				txtnewSubGroupName.setText("");
				btnaddmultipleaccounts.setEnabled(false);
				dropdownGroupName.setFocus();
				txtTotalCrOpeningBalance.setVisible(true);
				txtTotalDrOpeningBalance.setVisible(true);
				lblTotalCrOpeningBalance.setVisible(true);
				lblTotalDrOpeningBalance.setVisible(true);
				lblDiffInOpeningBalance.setVisible(true);
				txtDiffInOpeningBalance.setVisible(true);
				lblcrdrblnc.setVisible(true);
				onceEdited = false;
			}
			else
			{
		
				MessageBox msg = new MessageBox(new Shell(), SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
				msg.setText("Warning!");
				msg.setMessage("The account could not be saved, please try again.");
				msg.open();
			}
			
			Double totalDrOpeningBalance = 0.00;
			Double totalCrOpeningBalance = 0.00;
			Double diffBalance = 0.00;
			totalCrOpeningBalance= accountController.getTotalCr();
			totalDrOpeningBalance = accountController.getTotalDr();
			txtTotalCrOpeningBalance.setText(nf.format(totalCrOpeningBalance));
			txtTotalDrOpeningBalance.setText( nf.format(totalDrOpeningBalance));
			diffBalance = totalDrOpeningBalance - totalCrOpeningBalance;
			
			if(diffBalance < 0 )
			{
				diffBalance = Math.abs(diffBalance);
			}
			Double val1=	diffBalance;
			txtDiffInOpeningBalance.setText(nf.format( Double.parseDouble(val1.toString())));
			if(totalDrOpeningBalance>totalCrOpeningBalance)
			{
				lblcrdrblnc.setText("Dr");
			}
			else if (totalCrOpeningBalance>totalDrOpeningBalance) 
			{
				lblcrdrblnc.setText("Cr");
			}
			else
			{
				lblcrdrblnc.setText("");
			}
			txtOpeningBalance.setText("0.00");						
		}
		});
			
			this.btnReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				dropdownGroupName.setFocus();
				btnaddmultipleaccounts.setSelection(false);
				dropdownGroupName.select(0);
				dropdownSubGroupName.select(0);
				txtAccountName.setText("");
				txtAccountCode.setText("");         
				txtOpeningBalance.setText("0.00");
				
				txtTotalDrOpeningBalance.setText("0.00");
				txtTotalCrOpeningBalance.setText("0.00");
				txtnewSubGroupName.setText("");
				txtnewSubGroupName.setVisible(false);
				lblnewSubGroupName.setVisible(false);
				btnaddmultipleaccounts.setEnabled(false);
				txtTotalCrOpeningBalance.setVisible(true);
				txtTotalDrOpeningBalance.setVisible(true);
				lblTotalCrOpeningBalance.setVisible(true);
				lblTotalDrOpeningBalance.setVisible(true);
				lblDiffInOpeningBalance.setVisible(true);
				txtDiffInOpeningBalance.setVisible(true);
				lblcrdrblnc.setVisible(true);
				
			}
		});
			
			txtOpeningBalance.addVerifyListener(new VerifyListener() {
				
				@Override
				
				public void verifyText(VerifyEvent arg0) {
					// TODO Auto-generated method stub
					if(verifyFlag== false)
					{
						arg0.doit= true;
						return;
					}
					if(arg0.keyCode==46)
					{
						return;
					}
					if(arg0.keyCode==45)
					{
						return;
					}
					switch (arg0.keyCode) {
		            case SWT.BS:           // Backspace
		            case SWT.DEL:          // Delete
		            case SWT.HOME:         // Home
		            case SWT.END:          // End
		            case SWT.ARROW_LEFT:   // Left arrow
		            case SWT.ARROW_RIGHT: // Right arrow
		            case SWT.KEYPAD_DECIMAL:
		                return;
		        }

		        if (!Character.isDigit(arg0.character)) {
		            arg0.doit = false;  // disallow the action
		        }

					

				}
			});
			}
	
	

	
	public void makeaccssible(Control c)
		{
			c.getAccessible();
		}
}